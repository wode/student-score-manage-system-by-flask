import pandas as pd


class DataManage:
    """
    数据管理
    """

    def __init__(self):
        self.data_file = 'score.xlsx'  # 指定数据存储文件
        self.data_frame = pd.read_excel(self.data_file)  # 读取文件的数据，加载到data_frame中，用来给其他地方调用

    def read_data(self):
        self.data_frame = pd.read_excel(self.data_file)  # 通过read_excel加载数据
        return self.data_frame

    def save_data(self):
        """
        保存数据
        :return:
        """
        self.data_frame.to_excel(self.data_file, sheet_name='Sheet1', index=False, header=True)#保存最新数据导excel文件中

    def add_data(self, college, major, t_lass, stu_card, stu_name, course, score):
        """
        新增数据
        :return:
        """
        last_index = self.data_frame.index[-1] + 1 if self.data_frame.index.size else 0 # 计算出最后一条数据的索引值
        self.data_frame.loc[last_index] = [college, major, t_lass, stu_card, stu_name, course, score]  # 新增一条数据，并指定对应的值
        self.save_data()
        print('新增成功')
        print(self.data_frame)


if __name__ == '__main__':
    """
    批量造一些随机的数据，方便测试
    """
    import random
    with open('names.txt', encoding='utf8') as f:
        names = f.read()
        names = names.split('、')

    dm = DataManage()
    # 造数据
    for i in range(1, 10):
        college = random.choice(['计算机学院', '信息技术学院', '信息工程学院'])
        major = random.choice(['计算机科学与技术', '网络工程', '信息安全（网络安全）', '软件工程''物联网工程', '智能科学与技术', '大数据', '电子信息工程'])
        t_lass = random.choice(['一班', '二班', '三班', '四班'])
        stu_card = random.randint(100000000, 999999999)
        stu_name = random.choice(names)
        course = random.choice(
            ['算机应用基础', '应用文写作', '数学', '英语', '德育', '电工与电子技术', '计算机网络技术', 'C语言', '计算机组装与维修', '企业网安全高级技术', '企业网综合管理',
             '局域网组建', 'Linux服务器操作系统', '网络设备与网络技术', '网络综合布线技术', 'CAD绘图等课程'])
        score = random.randint(40, 100)
        dm.add_data(college, major, t_lass, stu_card, stu_name, course, score)